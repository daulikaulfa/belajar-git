<?php
  function cargar(){
    $consulta = new Consulta();
    $filas = $consulta->cargarProductos();
    echo '<table border="1" cellpading="10" cellspacing="5" align="center">
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>Jurusan</th>
            <th>NIS</th>
            <th>Keterangan</th>
          </tr>';
    foreach ($filas as $fila) {
      echo '<tr>';
      echo '<td>'. $fila['id_producto'] .'</td>';
      echo '<td>'. $fila['nombre'] .'</td>';
      echo '<td>'. $fila['descripcion'] .'</td>';
      echo '<td>'. $fila['categoria'] .'</td>';
      echo '<td>'. $fila['precio'] .'</td>';      
      echo "<td><a href='modificar.php?id_producto=". $fila['id_producto'] ."'> 
Ubah  ";
      echo "<a href='../kontrol/eliminar.php?id_producto=". $fila['id_producto'] ."'> 
Hapus  </td>";
      echo '</tr>';
    }
    echo '</table>';
  }

  function buscar($nombre){
    $consulta = new Consulta();
    $filas = $consulta->buscarProductos($nombre);
    echo '<table border="1" cellpading="10" cellspacing="5" align="center">
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>Jurusan</th>
            <th>NIS</th>
            <th>Keterangan</th>            
          </tr>';
    if (isset($filas)) {
      foreach ($filas as $fila) {
        echo '<tr>';
        echo '<td>'. $fila['id_producto'] .'</td>';
        echo '<td>'. $fila['nombre'] .'</td>';
        echo '<td>'. $fila['descripcion'] .'</td>';
        echo '<td>'. $fila['categoria'] .'</td>';
        echo '<td>'. $fila['precio'] .'</td>';        
        echo "<td><a href='modificar.php?id_producto=". $fila['id_producto'] ."'> Ubah  ";
        echo "<a href='../kontrol/eliminar.php?id_producto=". $fila['id_producto'] ."'> 
Hapus  </td>";
        echo '</tr>';
      }
    }
    echo '</table>';
  }

?>
