<?php

class Consulta {

  public function insertarProducto ($nama, $deskripsi, $kategori, $harga){
      try {

          $modelo = New Conexion();
          $conexion = $modelo->openConexion();
          $sql = "INSERT INTO produk (id_produk, nama, deskripsi, kategori, harga) values (DEFAULT,?,?,?,?)";
          $statement = $conexion->prepare($sql);
          $statement->bindParam(1,$nama);
          $statement->bindParam(2,$deskripsi);
          $statement->bindParam(3,$kategori);
          $statement->bindParam(4,$harga);
          $statement->execute(); 
          if (!$statement) {
              return '
Kesalahan saat membuat registri';
          } else {
              
              return '
Daftar Dibuat dengan Benar';
          }
          $modelo->closeConexion();
      } catch (PDOException $e) {
          $e->getMessage();
      }
  }    

  public function cargarProductos(){
    try {
        $rows = null;
        $modelo = New Conexion();
        $conexion = $modelo->openConexion();
        $sql = "SELECT * FROM produk";
        $statement = $conexion->prepare($sql);
        $statement->execute();
        $modelo->closeConexion();

        return $statement->fetchAll();          
      } catch (PDOException $e) {
          $e->getMessage();
      }     
  }

  public function eliminarProducto($id_produk){
     try {
          $modelo = new Conexion();
          $conexion = $modelo->openConexion();
          $sql = 'DELETE FROM produk WHERE id_produk = ?';
          $statement = $conexion->prepare($sql);
          $statement->bindParam(1, $id_produk);
          $statement->execute();
          if (!$statement) {
            return 'Kesalahan saat menghapus registri';
          }else{              
            return 'Registri berhasil dihapus';
          }
          $modelo->closeConexion();
      } catch (PDOException $e) {
          $e->getMessage();
      }      
  }

  public function buscarProductos($_nama){
      try {
          $rows = null;
          $modelo = New Conexion();
          $nama = '%'.$_nama.'%';
          $conexion = $modelo->openConexion();
          $sql = 'SELECT * FROM produk WHERE nama LIKE ?';
          $statement =  $conexion->prepare($sql);
          $statement->bindParam(1,$nama);
          $statement->execute();
          while ($result = $statement->fetch()) {
            $rows[] = $result;
          }
          //var_dump ($rows);
          return $rows;
          $modelo->closeConexion();
      } catch (PDOException $e) {
          $e->getMessage();
      }
  }

  public function filtrarProductoID($id){
      try {
          $rows = null;
          $modelo = New Conexion();
          $conexion = $modelo->openConexion();
          $sql = 'SELECT * FROM produk WHERE id_produk = ?';
          $statement =  $conexion->prepare($sql);
          $statement->bindParam(1,$id);
          $statement->execute();
          while ($result = $statement->fetch()) {
            $rows[] = $result;
          }
          //var_dump ($rows);
          return $rows;
          $modelo->closeConexion();
      } catch (PDOException $e) {
          $e->getMessage();
      }
  }      

  public function modificarProducto($campo, $valor, $id){
     try {
          $rows = null;
          $modelo = New Conexion();
          $conexion = $modelo->openConexion();
          $sql = "UPDATE produk SET $campo = ? WHERE id_produk = ?";
          $statement = $conexion->prepare($sql);
          $statement->bindParam(1,$valor);
          $statement->bindParam(2,$id);
          if (!$statement) {
            return 'Kesalahan memodifikasi produk';
          }else{
          $statement->execute();
          return 'Pendaftaran Dimodifikasi dengan benar';
          }
          $modelo->closeConexion();
      } catch (PDOException $e) {
          $e->getMessage();
      }
  }

}


?>
