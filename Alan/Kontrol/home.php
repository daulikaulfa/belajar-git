<?php
  function cargar(){
    $consulta = new Consulta();
    $filas = $consulta->cargarProductos();
    echo '<table border="2" cellpading="5" cellspacing="5">
          <tr>
            <th>ID</th>
            <th>Nama</th>
            <th>Deskripsi</th>
            <th>Kategori</th>
            <th>Harga</th>
            <th>Tindakan</th>
          </tr>';
    foreach ($filas as $fila) {
      echo '<tr>';
      echo '<td>'. $fila['id_produk'] .'</td>';
      echo '<td>'. $fila['nama'] .'</td>';
      echo '<td>'. $fila['deskripsi'] .'</td>';
      echo '<td>'. $fila['kategori'] .'</td>';
      echo '<td>'. $fila['harga'] .'</td>';      
      echo "<td><a href='modificar.php?id_produk=". $fila['id_produk'] ."'> 
Ubah  ";
      echo "<a href='../kontrol/eliminar.php?id_produk=". $fila['id_produk'] ."'> 
Hapus  </td>";
      echo '</tr>';
    }
    echo '</table>';
  }

  function buscar($nama){
    $consulta = new Consulta();
    $filas = $consulta->buscarProductos($nama);
    echo '<table border="2" cellpading="5" cellspacing="5">
          <tr>
            <th>ID</th>
            <th>Nama</th>
            <th>Deskripsi</th>
            <th>Kategori</th>
            <th>Harga</th>
            <th>Tindakan</th>            
          </tr>';
    if (isset($filas)) {
      foreach ($filas as $fila) {
        echo '<tr>';
        echo '<td>'. $fila['id_produk'] .'</td>';
        echo '<td>'. $fila['nama'] .'</td>';
        echo '<td>'. $fila['deskripsi'] .'</td>';
        echo '<td>'. $fila['kategori'] .'</td>';
        echo '<td>'. $fila['harga'] .'</td>';        
        echo "<td><a href='modificar.php?id_produk=". $fila['id_produk'] ."'> Ubah  ";
        echo "<a href='../kontrol/eliminar.php?id_produk=". $fila['id_produk'] ."'> 
Hapus  </td>";
        echo '</tr>';
      }
    }
    echo '</table>';
  }

?>
