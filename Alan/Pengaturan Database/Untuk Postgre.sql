CREATE TABLE public.produk (
  id_produk SERIAL PRIMARY KEY,
  nama VARCHAR(100) NOT NULL,
  deskripsi VARCHAR(500) NOT NULL,
  kategori VARCHAR(50) NOT NULL,
  harga INTEGER
);

INSERT INTO produk 
(id_produk, nama, deskripsi, kategori, harga) 
VALUES (DEFAULT,mangga,manis,buah,5200);